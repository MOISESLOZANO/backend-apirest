package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel getProductoId(@RequestBody ProductoModel newProducto) {
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos")
    public boolean putProductos(@RequestBody ProductoModel productoTOUpdate)
    {
        if (productoService.findById(productoTOUpdate.getId()).isPresent()) {
            productoService.save(productoTOUpdate);
            return true;
        }
        else {
            return false;
        }
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductos(@PathVariable String id) {
        if (productoService.existsById(id))
        {
            productoService.deledeById(id);
            return true;
        }
        else {
            return false;
        }
    }

    /*public boolean deleteProductos(@RequestBody ProductoModel productoToDelete)
    {//ANTIGUO DELETE
        try {
            productoService.delete(productoToDelete);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }*/

    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody Map<String, Object> update, @PathVariable String id)
    {
        ProductoModel productoAPatchear;

        Optional<ProductoModel> productonull = productoService.findById(id);
        if (productonull.isPresent())
        {
            productoAPatchear = productonull.get();
        }
        else
            return new ProductoModel("0","producto nulo",0.0);

        if (update.containsKey("precio"))
            productoAPatchear.setPrecio((Double) update.get("precio"));
        if (update.containsKey("descripcion"))
            productoAPatchear.setDescripcion((String) update.get("descripcion"));

        productoService.save(productoAPatchear);
        return productoAPatchear;
    }
}
